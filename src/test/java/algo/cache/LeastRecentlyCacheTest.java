package algo.cache;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class LeastRecentlyCacheTest {

    @Test
    public void test_1() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        assertArrayEquals(new Integer[] {5, 4, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_2() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        cache.get(1);

        assertArrayEquals(new Integer[] {1, 5, 4, 3, 2}, cache.keys().toArray());
    }

    @Test
    public void test_3() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        cache.get(2);

        assertArrayEquals(new Integer[] {2, 5, 4, 3, 1}, cache.keys().toArray());
    }

    @Test
    public void test_4() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        cache.get(4);

        assertArrayEquals(new Integer[] {4, 5, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_5() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        cache.get(5);

        assertArrayEquals(new Integer[] {5, 4, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_6() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        cache.get(5);
        cache.get(4);
        cache.get(3);
        cache.get(2);
        cache.get(1);

        assertArrayEquals(new Integer[] {1, 2, 3, 4, 5}, cache.keys().toArray());
    }

    @Test
    public void test_7() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        cache.get(4);
        cache.get(2);

        assertArrayEquals(new Integer[] {2, 4, 5, 3, 1}, cache.keys().toArray());
    }

    @Test
    public void test_8() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");

        cache.get(1);

        assertArrayEquals(new Integer[] {1}, cache.keys().toArray());
    }

    @Test
    public void test_9() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();


        assertArrayEquals(new Integer[] {}, cache.keys().toArray());
    }

    @Test
    public void test_10() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");

        cache.get(1);

        assertArrayEquals(new Integer[] {1, 2}, cache.keys().toArray());
    }

    @Test
    public void test_11() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        assertArrayEquals(new Integer[] {11, 21, 5, 4, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_12() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        cache.remove(3);

        assertArrayEquals(new Integer[] {11, 21, 5, 4, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_13() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");

        cache.remove(21);

        assertArrayEquals(new Integer[] {11, 5, 4, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_14() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        cache.remove(21);

        assertArrayEquals(new Integer[] {11, 5, 4, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_15() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        cache.remove(11);

        assertArrayEquals(new Integer[] {21, 5, 4, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_16() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        cache.remove(11);
        cache.remove(21);

        assertArrayEquals(new Integer[] {5, 4, 3, 2, 1}, cache.keys().toArray());
    }

    @Test
    public void test_17() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        cache.remove(11);
        cache.remove(21);
        cache.remove(1);

        assertArrayEquals(new Integer[] {5, 4, 3, 2}, cache.keys().toArray());
    }

    @Test
    public void test_18() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        cache.remove(11);
        cache.remove(21);
        cache.remove(1);
        cache.remove(5);
        cache.remove(4);
        cache.remove(3);

        assertArrayEquals(new Integer[] {2}, cache.keys().toArray());
    }

    @Test
    public void test_19() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>();
        cache.put(1, "1");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");
        cache.put(11, "11");
        cache.put(21, "21");
        cache.put(11, "111");

        cache.remove(11);
        cache.remove(21);
        cache.remove(1);
        cache.remove(5);
        cache.remove(2);
        cache.remove(4);
        cache.remove(3);

        assertArrayEquals(new Integer[] {}, cache.keys().toArray());
    }

    @Test
    public void test_20() {
        LeastRecentlyCache<Integer, String> cache = new LeastRecentlyCache<>(5);
        cache.put(1, "soft");
        cache.put(2, "2");
        cache.put(3, "3");
        cache.put(4, "4");
        cache.put(5, "5");

        assertArrayEquals(new String[] {"5", "4", "3", "2", "soft"}, cache.values().toArray());

        cache.put(6, "6");

        assertArrayEquals(new String[] {"6", "5", "4", "3", "2"}, cache.values().toArray());

        assertEquals("soft", cache.get(1));
    }
}
