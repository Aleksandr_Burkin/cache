package algo.cache;

import java.util.Collection;

/**
 *
 * @param <K> - key
 * @param <V> - value
 *
 * @author Aleksandr Burkin
 */
public interface Cache<K, V> {

    void put(K key, V value);

    V get(Object key);

    boolean remove(Object key);

    Collection<K> keys();

    Collection<V> values();
}
