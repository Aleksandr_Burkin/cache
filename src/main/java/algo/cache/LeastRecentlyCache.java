package algo.cache;

import java.lang.ref.SoftReference;
import java.util.*;

/**
 *
 * LRU-cache (least recently used) with temporary storage support of old data
 *
 * @param <K> - key
 * @param <V> - value
 *
 * @author Aleksandr Burkin
 */
public class LeastRecentlyCache<K, V> implements Cache<K, V> {

    private static final int DEFAULT_CAPACITY = 10;
    private static final int DEFAULT_SOFT_CAPACITY = 1000;

    private int size;

    private int mod;

    private int softMod;

    private Node[] store;

    private RankNode headrank;

    private RankNode tailrank;

    private SoftReference<V>[] softrefs;

    private long modCount;

    private static class Node<K, V> {
        K key;
        V value;
        Node next;
        RankNode rank;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private static class RankNode<K, V> {
        K key;
        V value;
        RankNode next;
        RankNode prev;

        RankNode(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    public LeastRecentlyCache() {
        this(DEFAULT_CAPACITY, DEFAULT_SOFT_CAPACITY);
    }

    public LeastRecentlyCache(int capacity) {
        if (capacity <= 0) throw new IllegalArgumentException("capacity cannot be zero");

        mod = capacity;
        softMod = DEFAULT_SOFT_CAPACITY;
        store = new Node[capacity];
        softrefs = new SoftReference[DEFAULT_SOFT_CAPACITY];
    }

    public LeastRecentlyCache(int capacity, int softcapacity) {
        if (capacity <= 0) throw new IllegalArgumentException("capacity cannot be zero");
        if (softcapacity <= 0) throw new IllegalArgumentException("softcapacity cannot be zero");

        mod = capacity;
        softMod = softcapacity;
        store = new Node[capacity];
        softrefs = new SoftReference[softcapacity];
    }

    private int hash(Object key) {
        return (key.hashCode() & 0x7fffffff) % mod;
    }

    private int softhash(Object key) {
        return (key.hashCode() & 0x7fffffff) % softMod;
    }

    @Override
    public void put(K key, V value) {
        int hash = hash(key);

        Node current = store[hash];
        Node previous = null;

        while (current != null) {
            if (current.key.equals(key)) {
                current.value = value;
                shiftNode(current.rank);

                return;
            }
            previous = current;
            current = current.next;
        }

        //otherwise, add a new node
        Node node = new Node(key, value);

        if (previous == null) store[hash] = node;
        else previous.next = node;

        RankNode rank = new RankNode(key, value);
        node.rank = rank;

        if (headrank == null) {
            headrank = rank;
            tailrank = rank;
        } else {
            rank.next = headrank;
            headrank.prev = rank;
            headrank = rank;
        }

        if (size == mod) {
            int softhash = softhash(tailrank.key);
            softrefs[softhash] = new SoftReference(tailrank.value);
            remove(tailrank.key);
        } else {
            size++;
        }

        modCount++;
    }

    @Override
    public V get(Object key) {
        int hash = hash(key);

        Node<K, V> current = store[hash];

        V result = null;

        while (current != null) {
            if (current.key.equals(key)) {
                shiftNode(current.rank);
                result = current.value;
                modCount++;
                break;
            }
            current = current.next;
        }

        if (result == null) {
            SoftReference<V> ref = softrefs[softhash(key)];
            if (ref != null) result = ref.get();
        }

        return result;
    }

    @Override
    public boolean remove(Object key) {
        int hash = hash(key);

        Node<K, V> current = store[hash];
        Node previous = null;

        while (current != null) {
            if (current.key.equals(key)) {
                RankNode rank = current.rank;

                if (--size != 0) {
                    if (rank == headrank) {
                        headrank = rank.next;
                        headrank.prev = null;
                    } else if (rank == tailrank) {
                        tailrank = rank.prev;
                        tailrank.next = null;
                    } else {
                        rank.prev.next = rank.next;
                        rank.next.prev = rank.prev;
                    }
                } else {
                    headrank = null;
                    tailrank = null;
                }

                if (previous == null) store[hash] = current.next;
                else previous.next = current.next;

                modCount++;

                return true;
            }
            previous = current;
            current = current.next;
        }

        return false;
    }

    private void shiftNode(RankNode rank) {
        if (rank != headrank) {
            if (tailrank == rank) tailrank = rank.prev;

            rank.prev.next = rank.next;
            if (rank.next != null) rank.next.prev = rank.prev;

            rank.prev = null;
            rank.next = headrank;
            headrank.prev = rank;
            headrank = rank;
        }
    }

    @Override
    public Collection<K> keys() {
        return new RankedKeys();
    }

    @Override
    public Collection<V> values() {
        return new RankedValues();
    }

    final class RankedKeys extends AbstractCollection<K> {

        @Override
        public Iterator<K> iterator() {

            return new Iterator<>() {

                private RankNode<K, V> current = headrank;

                private long expectedModCount = modCount;

                @Override
                public boolean hasNext() {
                    return current != null;
                }

                @Override
                public K next() {
                    RankNode<K, V> node = current;

                    if (modCount != expectedModCount) throw new ConcurrentModificationException();
                    if (node == null) throw new NoSuchElementException();

                    current = node.next;

                    return node.key;
                }
            };
        }

        @Override
        public int size() {
            return size;
        }
    }

    final class RankedValues extends AbstractCollection<V> {

        @Override
        public Iterator<V> iterator() {
            return new Iterator<>() {

                private RankNode<K, V> current = headrank;

                private long expectedModCount = modCount;

                @Override
                public boolean hasNext() {
                    return current != null;
                }

                @Override
                public V next() {
                    RankNode<K, V> node = current;

                    if (modCount != expectedModCount) throw new ConcurrentModificationException();
                    if (node == null) throw new NoSuchElementException();

                    current = node.next;

                    return node.value;
                }
            };
        }

        @Override
        public int size() {
            return size;
        }
    }
}
